<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('table_order_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_id'); // Khóa ngoại đến bảng đơn hàng
            $table->unsignedBigInteger('product_id'); // Khóa ngoại đến bảng sản phẩm
            $table->integer('quantity');
            $table->decimal('unit_price', 10, 2);
            $table->timestamps();

            // Định nghĩa các khóa ngoại
            $table->foreign('order_id')->references('id')->on('table_orders');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('table_order_items');
    }
};
