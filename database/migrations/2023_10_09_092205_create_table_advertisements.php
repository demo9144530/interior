<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('table_advertisements', function (Blueprint $table) {
            $table->id();
            $table->string('title'); // Tiêu đề quảng cáo
            $table->text('description'); // Mô tả quảng cáo
            $table->string('image_path'); // Đường dẫn tới hình ảnh quảng cáo
            $table->date('start_date'); // Ngày bắt đầu hiển thị quảng cáo
            $table->date('end_date'); // Ngày kết thúc hiển thị quảng cáo
            $table->boolean('is_active')->default(true); // Trạng thái hoạt động của quảng cáo

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('table_advertisements');
    }
};
