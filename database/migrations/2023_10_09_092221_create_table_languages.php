<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('table_languages', function (Blueprint $table) {
            $table->id();
            $table->string('name'); // Tên ngôn ngữ (ví dụ: Tiếng Anh, Tiếng Việt)
            $table->string('code'); // Mã ngôn ngữ (ví dụ: en, vi)
            $table->boolean('is_default')->default(false); // Ngôn ngữ mặc định
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('table_languages');
    }
};
