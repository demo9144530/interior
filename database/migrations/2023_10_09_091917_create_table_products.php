<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('status')->default(0);
            $table->decimal('price', 10, 2);
            $table->text('content');
            $table->integer('quantity');
            $table->integer('category_id');
            $table->string('slug')->unique();
            $table->string('createdBy')->nullable();
            $table->string('updatedBy')->nullable();
            $table->string('image')->nullable()->default(null);
            $table->text('detail')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};